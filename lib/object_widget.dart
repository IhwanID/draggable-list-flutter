import 'package:floorplan/cubit/floor_plan_cubit.dart';
import 'package:floorplan/table.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

class ObjectWidget extends StatefulWidget {
   ObjectWidget({super.key, required this.table});
   TableObject table;
  @override
  State<ObjectWidget> createState() => _ObjectWidgetState();
}

class _ObjectWidgetState extends State<ObjectWidget> {
  Widget buildWidget() {
    return Container(
        width: 100,
        height: 100,
        decoration: BoxDecoration(
          color: Colors.grey,
          borderRadius: BorderRadius.circular(8),
        ),
        child: Center(
          child: Text(
            "Table ${widget.table.id}",
            style: const TextStyle(
              color: Colors.white,
              fontSize: 24,
            ),
          ),
        ));
  }

  @override
  Widget build(BuildContext context) {
    var cubit = BlocProvider.of<FloorPlanCubit>(context);
    return Positioned(
     
      left: widget.table.x,
      top: widget.table.y,
      child: Draggable(
          feedback: Material(child: buildWidget()),
          childWhenDragging: Container(),
          onDragEnd: (dragDetails) {
            var table = widget.table;
            
            table.x = dragDetails.offset.dx.roundToNearest(25);
            table.y = dragDetails.offset.dy.roundToNearest(25);
            cubit.updateTable(table);
          },
          child: buildWidget()),
    );
  }

  
}

extension DoubleExtensions on double {
  double roundToNearest(double multiple) {
    double remainder = this % multiple;
    if (remainder < multiple / 2) {
      return this - remainder;
    } else {
      return this + (multiple - remainder);
    }
  }
}