import 'package:floorplan/cubit/floor_plan_cubit.dart';
import 'package:floorplan/object_widget.dart';
import 'package:floorplan/table.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

void main() {
  runApp(BlocProvider(
    create: (context) => FloorPlanCubit(),
    child: const MainApp(),
  ));
}

class MainApp extends StatelessWidget {
  const MainApp({super.key});

  @override
  Widget build(BuildContext context) {
    return const MaterialApp(
      home: Scaffold(
        body: Stack(
          children: [
            FloorPlanScreen(),
          ],
        ),
      ),
    );
  }
}

class FloorPlanScreen extends StatefulWidget {
  const FloorPlanScreen({super.key});

  @override
  State<FloorPlanScreen> createState() => _FloorPlanScreenState();
}

class _FloorPlanScreenState extends State<FloorPlanScreen> {
  @override
  Widget build(BuildContext context) {
    var cubit = BlocProvider.of<FloorPlanCubit>(context);
    return Scaffold(
      floatingActionButton: FloatingActionButton(
        onPressed: () {
          var tableNumber = cubit.state.tables.length + 1;
          cubit.addTable(TableObject(tableNumber,  0, 0));
        },
        child: const Icon(Icons.add),
      ),
      body: BlocListener<FloorPlanCubit, FloorPlanState>(
        listener: (context, state) {
          print(state.tables.map((e) => "${e.id},x: ${e.x}, y: ${e.y}").toList());
        },
        child: BlocBuilder<FloorPlanCubit, FloorPlanState>(
          builder: (context, state) {
            return Stack(
              children: [
                for (int x = 0; x < state.tables.length; x++) ...[
                  ObjectWidget(table: state.tables[x])
                ]
              ],
            );
          },
        ),
      ),
    );
  }
}
