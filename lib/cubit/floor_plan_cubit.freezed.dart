// coverage:ignore-file
// GENERATED CODE - DO NOT MODIFY BY HAND
// ignore_for_file: type=lint
// ignore_for_file: unused_element, deprecated_member_use, deprecated_member_use_from_same_package, use_function_type_syntax_for_parameters, unnecessary_const, avoid_init_to_null, invalid_override_different_default_values_named, prefer_expression_function_bodies, annotate_overrides, invalid_annotation_target, unnecessary_question_mark

part of 'floor_plan_cubit.dart';

// **************************************************************************
// FreezedGenerator
// **************************************************************************

T _$identity<T>(T value) => value;

final _privateConstructorUsedError = UnsupportedError(
    'It seems like you constructed your class using `MyClass._()`. This constructor is only meant to be used by freezed and you are not supposed to need it nor use it.\nPlease check the documentation here for more information: https://github.com/rrousselGit/freezed#custom-getters-and-methods');

/// @nodoc
mixin _$FloorPlanState {
  List<TableObject> get tables => throw _privateConstructorUsedError;
  bool get isLoading => throw _privateConstructorUsedError;

  @JsonKey(ignore: true)
  $FloorPlanStateCopyWith<FloorPlanState> get copyWith =>
      throw _privateConstructorUsedError;
}

/// @nodoc
abstract class $FloorPlanStateCopyWith<$Res> {
  factory $FloorPlanStateCopyWith(
          FloorPlanState value, $Res Function(FloorPlanState) then) =
      _$FloorPlanStateCopyWithImpl<$Res, FloorPlanState>;
  @useResult
  $Res call({List<TableObject> tables, bool isLoading});
}

/// @nodoc
class _$FloorPlanStateCopyWithImpl<$Res, $Val extends FloorPlanState>
    implements $FloorPlanStateCopyWith<$Res> {
  _$FloorPlanStateCopyWithImpl(this._value, this._then);

  // ignore: unused_field
  final $Val _value;
  // ignore: unused_field
  final $Res Function($Val) _then;

  @pragma('vm:prefer-inline')
  @override
  $Res call({
    Object? tables = null,
    Object? isLoading = null,
  }) {
    return _then(_value.copyWith(
      tables: null == tables
          ? _value.tables
          : tables // ignore: cast_nullable_to_non_nullable
              as List<TableObject>,
      isLoading: null == isLoading
          ? _value.isLoading
          : isLoading // ignore: cast_nullable_to_non_nullable
              as bool,
    ) as $Val);
  }
}

/// @nodoc
abstract class _$$FloorPlanStateImplCopyWith<$Res>
    implements $FloorPlanStateCopyWith<$Res> {
  factory _$$FloorPlanStateImplCopyWith(_$FloorPlanStateImpl value,
          $Res Function(_$FloorPlanStateImpl) then) =
      __$$FloorPlanStateImplCopyWithImpl<$Res>;
  @override
  @useResult
  $Res call({List<TableObject> tables, bool isLoading});
}

/// @nodoc
class __$$FloorPlanStateImplCopyWithImpl<$Res>
    extends _$FloorPlanStateCopyWithImpl<$Res, _$FloorPlanStateImpl>
    implements _$$FloorPlanStateImplCopyWith<$Res> {
  __$$FloorPlanStateImplCopyWithImpl(
      _$FloorPlanStateImpl _value, $Res Function(_$FloorPlanStateImpl) _then)
      : super(_value, _then);

  @pragma('vm:prefer-inline')
  @override
  $Res call({
    Object? tables = null,
    Object? isLoading = null,
  }) {
    return _then(_$FloorPlanStateImpl(
      tables: null == tables
          ? _value._tables
          : tables // ignore: cast_nullable_to_non_nullable
              as List<TableObject>,
      isLoading: null == isLoading
          ? _value.isLoading
          : isLoading // ignore: cast_nullable_to_non_nullable
              as bool,
    ));
  }
}

/// @nodoc

class _$FloorPlanStateImpl implements _FloorPlanState {
  const _$FloorPlanStateImpl(
      {final List<TableObject> tables = const [], this.isLoading = false})
      : _tables = tables;

  final List<TableObject> _tables;
  @override
  @JsonKey()
  List<TableObject> get tables {
    if (_tables is EqualUnmodifiableListView) return _tables;
    // ignore: implicit_dynamic_type
    return EqualUnmodifiableListView(_tables);
  }

  @override
  @JsonKey()
  final bool isLoading;

  @override
  String toString() {
    return 'FloorPlanState(tables: $tables, isLoading: $isLoading)';
  }

  @override
  bool operator ==(Object other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType &&
            other is _$FloorPlanStateImpl &&
            const DeepCollectionEquality().equals(other._tables, _tables) &&
            (identical(other.isLoading, isLoading) ||
                other.isLoading == isLoading));
  }

  @override
  int get hashCode => Object.hash(
      runtimeType, const DeepCollectionEquality().hash(_tables), isLoading);

  @JsonKey(ignore: true)
  @override
  @pragma('vm:prefer-inline')
  _$$FloorPlanStateImplCopyWith<_$FloorPlanStateImpl> get copyWith =>
      __$$FloorPlanStateImplCopyWithImpl<_$FloorPlanStateImpl>(
          this, _$identity);
}

abstract class _FloorPlanState implements FloorPlanState {
  const factory _FloorPlanState(
      {final List<TableObject> tables,
      final bool isLoading}) = _$FloorPlanStateImpl;

  @override
  List<TableObject> get tables;
  @override
  bool get isLoading;
  @override
  @JsonKey(ignore: true)
  _$$FloorPlanStateImplCopyWith<_$FloorPlanStateImpl> get copyWith =>
      throw _privateConstructorUsedError;
}
