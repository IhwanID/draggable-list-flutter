import 'package:bloc/bloc.dart';
import 'package:floorplan/table.dart';
import 'package:freezed_annotation/freezed_annotation.dart';

part 'floor_plan_state.dart';
part 'floor_plan_cubit.freezed.dart';

class FloorPlanCubit extends Cubit<FloorPlanState> {
  FloorPlanCubit() : super(const FloorPlanState());

  void addTable(TableObject table) {
    emit(state.copyWith(
      tables: [...state.tables, table],
    ));
  }

  void updateTable(TableObject table) {
    emit(state.copyWith(
      isLoading: true,
    ));
    var tables = state.tables.map((e) => e.id == table.id ? table : e).toList();
    emit(state.copyWith(
      tables: tables,
      isLoading: false,
    ));
  }
}
