part of 'floor_plan_cubit.dart';

@freezed
class FloorPlanState with _$FloorPlanState {
  const factory FloorPlanState({
   @Default([]) List<TableObject> tables,
   @Default(false) bool isLoading
  }) = _FloorPlanState;
}
